import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location
from django.views.decorators.http import require_http_methods
from .models import State
from .acls import get_photo, get_weather_data


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name"
    ]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        # Create a new Conference using the updated content
        conference = Conference.objects.create(**content)

        # Return the created Conference as a JsonResponse
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        weather = get_weather_data(
                conference.location.city,
                conference.location.state)

        return JsonResponse(
            {
                "conference": conference,
                "weather": weather,
            },
            encoder=ConferenceDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    elif request.method == "PUT":
        content = json.loads(request.body)

        # Update the location if provided
        location_id = content.get("location")
        if location_id:
            try:
                location = Location.objects.get(id=location_id)
                content["location"] = location
            except Location.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400,
                )

        # Update the Conference with the new content
        Conference.objects.filter(id=id).update(**content)

        # Fetch the updated Conference and return it
        updated_conference = Conference.objects.get(id=id)
        return JsonResponse(
            updated_conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    return JsonResponse(
        {"message": "Invalid HTTP method"},
        status=400
    )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the State object and put it in the content dict
        state_abbreviation = content.get("state")
        if state_abbreviation:
            try:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
            except State.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid state abbreviation"},
                    status=400,
                )

        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

        # Update the state if provided
        state_abbreviation = content.get("state")
        if state_abbreviation:
            try:
                state = State.objects.get(abbreviation=state_abbreviation)
                content["state"] = state
            except State.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid state abbreviation"},
                    status=400,
                )

        # Update the Location with the new content
        Location.objects.filter(id=id).update(**content)

        # Fetch the updated Location and return it
        updated_location = Location.objects.get(id=id)
        return JsonResponse(
            updated_location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
