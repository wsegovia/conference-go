from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


# Function to get a photo using the Pexels API
def get_photo(city, state):
    # Define the URL for the Pexels search API
    url = "https://api.pexels.com/v1/search"
    # Set headers for the API request (including Pexels API key)
    headers = {"Authorization": PEXELS_API_KEY}
    # Set parameters for the API request to the Pexels search endpoint
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    # Make a GET request to the Pexels API
    response = requests.get(url, params=params, headers=headers)
    # Parse the JSON content of the response
    content = json.loads(response.content)

    try:
        # Extract the URL of the first photo from the Pexels response
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        # Handle exceptions if the required data is not present in the response
        return {"picture_url": None}


# Function to get weather data using OpenWeatherMap API
def get_weather_data(city, state):
    # Define the URL for the geocoding API
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # Set parameters for the first API request to geocoding endpoint
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }

    # Make a GET request to the geocoding API
    response = requests.get(url, params=params)
    # Parse the JSON content of the response
    content = json.loads(response.content)

    try:
        # Extract latitude and longitude from the geocoding response
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        # Handle exceptions if the required data is not present in the response
        return None

    # Define the URL for the current weather API
    url = "https://api.openweathermap.org/data/2.5/weather"
    # Set parameters for the second API request to current weather endpoint
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",  # Use imperial units for temperature
    }

    # Make a GET request to the current weather API
    response = requests.get(url, params=params)
    # Parse the JSON content of the response
    content = json.loads(response.content)

    try:
        # Extract weather description and temperature from the weather response
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        # Handle exceptions if the required data is not present in the response
        return None
